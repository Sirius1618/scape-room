#include <SDL2/SDL.h>
#include <SDL2/SDL_render.h>
#include <time.h>
#include "../include/janela.h"

void rederJanela(Janela *janela) {

  if (janela != NULL && janela->render != NULL) {
    SDL_RenderClear(janela->render); 
  }
}
