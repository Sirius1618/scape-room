#include "../include/janela.h"
#include <SDL2/SDL_timer.h>
#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h> 
#include <time.h>

Janela* criarJanela(const char *title, int largura, int altura){ 

  // Aloca memoria para a crianção da janela. 
  Janela *janela = malloc(sizeof(Janela)); 
  if(janela == NULL) {
    printf("Erro: falha no alocamento de mémoria para janela\n"); 
    return NULL; 
  }

  // Criando janela 
  janela->janela = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, largura, altura, SDL_WINDOW_SHOWN); 
  if (janela->janela == NULL) {
    printf("Erro: falha ao inicializar SDL %s\n", SDL_GetError()); 
    free(janela); 
    return NULL; 
  }

  // Criando o render que inicia no drive -1 e para ser usado em harsware acelerado. 
  janela->render = SDL_CreateRenderer(janela->janela, -1, SDL_RENDERER_ACCELERATED); 
  if (janela->render == NULL) {
    printf("Erro: falha ao criar o renderizador SDL %s\n", SDL_GetError()); 
    SDL_DestroyRenderer(janela->render); 
    free(janela); 
    return NULL; 
  }
  
  janela->fechar = 1; 
  janela->altura = altura; 
  janela->largura = largura; 

  return janela; 
}
