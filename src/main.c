#include "../include/janela.h"
#include "criar_janela.c"
#include "destruir_janela.c"
#include "eventos_janela.c"

int main(int argc, char *argv[])
{

  Janela *janelaTeste; 
  janelaTeste = criarJanela("teste", 400, 400); 
  TipoRenderizacao tipoRenderizacao = RENDER_PADRAO; // Por exemplo, renderização padrão
  janelaTeste->fechar = 1; 

    // Loop principal do jogo
    SDL_Event evento;
    while (janelaTeste->fechar != 0) {
        // Processa eventos da janela
        while (SDL_PollEvent(&evento)) {
            // Chama a função handleWindowEvents para processar os eventos da janela
            eventosJanela(janelaTeste, evento, tipoRenderizacao);
        }

        // Lógica do jogo
        // ...

        // Renderiza a cena
        // ...

        // Aguarda um curto período de tempo para controlar a taxa de quadros
        SDL_Delay(16); // Aproximadamente 60 quadros por segundo
    }
  destruirJanela(janelaTeste); 
  return 0;
}
