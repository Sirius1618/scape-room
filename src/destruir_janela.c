#include "../include/janela.h"
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_video.h>
#include <stdio.h>
#include <SDL2/SDL.h>
#include <stdlib.h> 
#include <time.h>

void destruirJanela(Janela *janela){

  if (janela != NULL) {

    if(janela->render != NULL){
      SDL_DestroyRenderer(janela->render); 
    }

    if (janela->janela != NULL) {
      SDL_DestroyWindow(janela->janela); 
    }
    
    free(janela); 
  }
  else {
    printf("Aviso: Tentativa de destruir uma janela nula\n"); 
  }
}

