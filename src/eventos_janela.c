
// Tenho que colocar o que cada reder vai fazer com base na sena que estamo// Tenho que colocar o que cada reder vai fazer com base na sena (no) que estamos



#include <SDL2/SDL.h>
#include "../include/janela.h" 

// Enumeração para representar os tipos de renderização
typedef enum {
    RENDER_PADRAO,
    RENDER_INVENTARIO,
    RENDER_TRANSICAO
} TipoRenderizacao;

void eventosJanela(Janela *janela, SDL_Event evento, TipoRenderizacao tipoRenderizacao) {
    // Verifica se a janela é válida
    if (janela == NULL) {
        return;
    }
      

       // Processa eventos de teclado
    if (evento.type == SDL_KEYDOWN) {

        // Verifica se a tecla pressionada é "ESC" ou "Q"
        if (evento.key.keysym.sym == SDLK_ESCAPE || evento.key.keysym.sym == SDLK_q) {
            // Fecha a janela
            // Colocar aviso se a pessoa realemnte quer fechar a janela 
            janela->fechar = 0;
        }
    }
    // Processa eventos com base no tipo de renderização
    switch (tipoRenderizacao) {
        case RENDER_PADRAO:
            // Processa eventos específicos para a renderização padrão
            // Aqui você pode lidar com eventos de teclado, mouse, etc. que são relevantes para a renderização padrão
            break;
        case RENDER_INVENTARIO:
            // Processa eventos específicos para a renderização do inventário
            // Aqui você pode lidar com eventos de teclado, mouse, etc. que são relevantes para a renderização do inventário
            break;
        case RENDER_TRANSICAO:
            // Processa eventos específicos para a renderização da transição entre cenários
            // Aqui você pode lidar com eventos de teclado, mouse, etc. que são relevantes para a renderização da transição
            break;
        // Adicione mais casos para outros tipos de renderização, se necessário
    }
}

