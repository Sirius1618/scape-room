
#include <stdio.h>
#include <stdlib.h>
#include "../include/grafo-cenarios.h"

// Função para criar um novo nó com base nas informações do quarto
Node* createNode(Room room) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    if (newNode == NULL) {
        fprintf(stderr, "Erro: Falha na alocação de memória para um novo nó.\n");
        exit(EXIT_FAILURE);
    }
    newNode->room = room;
    newNode->edges = NULL;
    return newNode;
}

// Função para adicionar uma aresta entre dois nós do grafo
void addEdge(Graph* graph, int src, int dest) {
    Edge* newEdge = (Edge*)malloc(sizeof(Edge));
    if (newEdge == NULL) {
        fprintf(stderr, "Erro: Falha na alocação de memória para uma nova aresta.\n");
        exit(EXIT_FAILURE);
    }
    newEdge->dest = dest;
    newEdge->next = graph->nodes[src].edges;
    graph->nodes[src].edges = newEdge;
}

// Função para criar um grafo com o número especificado de quartos e algumas conexões predefinidas entre eles
Graph* createGraph(int numRooms) {
    Graph* graph = (Graph*)malloc(sizeof(Graph));
    if (graph == NULL) {
        fprintf(stderr, "Erro: Falha na alocação de memória para o grafo.\n");
        exit(EXIT_FAILURE);
    }
    graph->nodes = (Node*)malloc(numRooms * sizeof(Node));
    if (graph->nodes == NULL) {
        fprintf(stderr, "Erro: Falha na alocação de memória para os nós do grafo.\n");
        exit(EXIT_FAILURE);
    }
    graph->numNodes = numRooms;

    for (int i = 0; i < numRooms; i++) {
        Room room;
        room.id = i;
        snprintf(room.name, sizeof(room.name), "Room %d", i + 1);
        graph->nodes[i] = *createNode(room);
    }

    addEdge(graph, 0, 1);
    addEdge(graph, 1, 2);
    addEdge(graph, 2, 0);

    return graph;
}

// Função para imprimir o grafo
void printGraph(Graph* graph) {
    for (int i = 0; i < graph->numNodes; i++) {
        Room room = graph->nodes[i].room;
        printf("Room %d (%s):\n", room.id, room.name);
        Edge* edge = graph->nodes[i].edges;
        while (edge != NULL) {
            printf("  -> Room %d\n", edge->dest);
            edge = edge->next;
        }
    }
}

