#ifndef JANELA_H 
# define JANELA_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_video.h>

typedef struct Janela {

  SDL_Window *janela;
  SDL_Renderer *render; 

  int fechar; 
  int largura; 
  int altura; 
} Janela; 

Janela* criarJanela(const char *title, int largura, int altura); 
void destruirJanela(Janela *janela); 
void rederJanela(Janela *janela); 
void lidarComEventosJanela(Janela *janela, SDL_Event evento); 

#endif 

