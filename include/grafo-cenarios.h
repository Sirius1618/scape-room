
#ifndef GRAPH_H
#define GRAPH_H

#include "room.h" // Inclui o cabeçalho de room.h para usar a estrutura Room

// Define a estrutura de uma aresta
typedef struct Edge {
    int dest;
    struct Edge* next;
} Edge;

// Define a estrutura de um nó do grafo
typedef struct {
    Room room; // Usa a estrutura Room definida em room.h
    Edge* edges;
} Node;

// Define a estrutura do grafo
typedef struct {
    Node* nodes;
    int numNodes;
} Graph;

// Protótipos das funções
Node* createNode(Room room);
void addEdge(Graph* graph, int src, int dest);
Graph* createGraph(int numRooms);
void printGraph(Graph* graph);

#endif /* GRAPH_H */
